# ciIndexOf

Case insensitive indexOf module for node.js

Usage:
require('ciindexof');
if(teststring.ciIndexOf('searchString') !== -1) {}
Returns index position of first letter of searchString, or -1 if not found.