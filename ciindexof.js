if(!String.prototype.ciIndexOf) {
	String.prototype.ciIndexOf = function (fNeedle, fOffset) {
		var hs = this.toLowerCase();
		if(fNeedle !== null && fNeedle !== undefined && fNeedle !== '') {
			var ne = fNeedle.toLowerCase();
			if(fOffset === undefined || fOffset === null || fOffset === '') {
				fOffset = 0;
			}
			return hs.indexOf(ne, fOffset);
		} else {
			return -1;
		}
	};
}